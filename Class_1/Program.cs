﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //creating an instance of the water bottle class

            water_bottle bottle_1 = new water_bottle();

            //displaying the contents of bottle 1
            //I am able to access the method show_details_about_bottle because it is public
            //also, they will show the default constructor assigned values
            bottle_1.show_details_about_bottle();

            //creating a second instance of water bottle class

            water_bottle bottle_2 = new water_bottle();
            //accessing the method that can set bottle values
            //sending two parameters
            bottle_2.set_bottle_values(10, 50);

            //now displaying
            bottle_2.show_details_about_bottle();

            //this line is here to stop the console window from dissapearing.
            Console.ReadLine();
        }
    }

    public class water_bottle
    {
        //private field. that means, it cannot be accessed from outside members
        private int color;
        //private field. that means, it cannot be accessed from outside members
        private int quantity;

        //constructor. the moment an instance of this class is created 
        //both color and quantity will have the values mentioned inside
        //notice how the constructor has no return type
        public water_bottle()
        {
            color = 2;
            quantity = 5;
        }

        //this is a public method and hence can be access from members outside the class
        public void show_details_about_bottle()
        {
            //the number in the brackets indicate the postion of the parameters on the right
            //half of the console statement
            Console.WriteLine("color is {0} quantity is {1} ", color, quantity);
        }

        //this is a private method. so it can only be accessed from inside members
        private void set_color(int color_sent)
        {
            color = color_sent;
        }

        //private method. see previous method explanation.
        private void set_quantity(int quantity_sent)
        {
            quantity = quantity_sent;
        }

        //I have to write this method if I wish to set my own color and quanity
        //the reasons are like this
        //both color and quantity are private fields. 
        //both the methods that set color and quanity are also private
        //this method can access the private methods that set color and quantity
        //as it is a member of this class
        public void set_bottle_values(int color, int quantity)
        {
            set_color(color);
            set_quantity(quantity);
        }


    }
}
